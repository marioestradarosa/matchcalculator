package cordova.plugin.matchcalculator;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

 
public class MatchCalculator extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("add")) {
            this.add(args, callbackContext);
            return true;
        }

        if (action.equals("substract")) {
            this.substract(args, callbackContext);
            return true;
        }
        return false;
    }


    private void add(JSONArray args, CallbackContext cb) {

        if (args != null){
            try {
                int p1 = Integer.parseInt(args.getJSONObject(0).getString("intA"));
                int p2 = Integer.parseInt(args.getJSONObject(0).getString("intB"));
                cb.success("" + (p1+p2));

            } catch (Exception err) {
                cb.error("Something went wrong " + err);

            }

        } else {
            cb.error("Please specify arguments");
        }
    }

    private void substract(JSONArray args, CallbackContext cb) {

        if (args != null){
            try {
                int p1 = Integer.parseInt(args.getJSONObject(0).getString("intA"));
                int p2 = Integer.parseInt(args.getJSONObject(0).getString("intB"));
                cb.success("" + (p1-p2));

            } catch (Exception err) {
                cb.error("Something went wrong " + err);

            }

        } else {
            cb.error("Please specify arguments");
        }
    }
}
