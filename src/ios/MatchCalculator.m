/********* MatchCalculator.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>

@interface MatchCalculator : CDVPlugin {
  // Member variables go here.
}

 - (void)add:(CDVInvokedUrlCommand*)command;
 - (void)substract:(CDVInvokedUrlCommand*)command;
 @end

@implementation MatchCalculator

 
- (void)add:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSNumber *p1 = [[command.arguments objectAtIndex:0] valueForKey:@"intA"];
    NSNumber *p2 = [[command.arguments objectAtIndex:0] vaueForKey:@"intB"];
 
   if (p1 > 0 && p2 >0) {
       NSString* total =  @(p1 + p2);
       pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:total];

   } else {
      pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];

   }
 
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}


- (void)substract:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSNumber *p1 = [[command.arguments objectAtIndex:0] valueForKey:@"intA"];
    NSNumber *p2 = [[command.arguments objectAtIndex:0] vaueForKey:@"intB"];
 
   if (p1 > 0 && p2 >0) {
       NSString* total =  @(p1 - p2);
       pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:total];

   } else {
      pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];

   }
 
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end
